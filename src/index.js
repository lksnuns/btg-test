const minBrokenBricks = require('./minBrokenBricks')
const fs = require('fs')

let data
try {
  data = fs.readFileSync('./input/wall.txt', 'utf8')
} catch (err) {

  if (err.code === 'ENOENT') {
    console.log('"./input/wall.txt" not found')
  } else {
    console.log(err)
  }

  process.exit(0)
}

let arr = JSON.parse(data)
let result = minBrokenBricks(arr)

console.log(result)
