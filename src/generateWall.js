
function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}


function generateWall(maxCol, rows, maxBricksSize = 7) {
  let size = 0
  let wall = []

  for (let i = 0; i < rows; i++) {
    size = 0
    let row = []
    let remainingSpace = maxCol + 1

    while (remainingSpace > maxBricksSize) {
      let brick = randomNumber(1, maxBricksSize)
      size += brick
      remainingSpace = maxCol - size

      row.push(brick)
    }

    row.push(remainingSpace)
    wall.push(row)
  }

  return wall
}

module.exports = generateWall
