/**
 * Finds the amount of gaps between bricks in each column of the wall
 *
 * usage:
 * ```js
 * let wall = [[1, 2, 2, 1], [3, 1, 2], [1, 3, 2], [2, 4], [3, 1, 2], [1, 3, 1, 1]]
 *
 * gapsByColumn(wall) === [ 3, 1, 3, 4, 2 ]
 * ````
 *
 * @param  {Array}   Array of arrays that represents the wall
 * @return {Array}   new Array with amount of gaps of each column
 */
function gapsByColumn(wall) {
  let gaps = new Array()
  let columnPosition = 0

  for (let bricksRow of wall) {
    columnPosition = -1
    let length = bricksRow.length

    // -1 prevents from checking the last column, where all bricks end
    for (let i = 0; i < length - 1; i++) {
      let brick = bricksRow[i]

      columnPosition += brick

      let amount = gaps[columnPosition] || 0
      gaps[columnPosition] = amount + 1
    }
  }

  return gaps
}

/**
 * Finds the column with the least number of brick breaks
 *
 * usage:
 * ```js
 * let wallHeight = 6
 * let gapsByColumn = [ 3, 1, 3, 4, 2 ]
 *
 * minBrokenBricksFromGaps(wallHeight, gapsByColumn) === 2
 *
 * ````
 * @param  {Number}    Height of wall
 * @param  {Array}     Array of gaps by columns
 * @return {Number}    The minimum break bricks possible
 */
function minBrokenBricksFromGaps(wallHeight, gaps) {
  const max = gaps.reduce((max, current) =>  (current > max) ? current : max, 0)

  return wallHeight - max
}


/**
 * Find the best path with the least amount of broken bricks
 *
 * usage:
 * ```js
 * let wall = [[1, 2, 2, 1], [3, 1, 2], [1, 3, 2], [2, 4], [3, 1, 2], [1, 3, 1, 1]]
 *
 * minBrokenBricks(wall) === 2
 *
 * ````
 *
 * @param  {Array}    Array of arrays that represents the wall
 * @return {Number}   The minimum break bricks possible
 */
function minBrokenBricks(wall) {
  let wallHeight = wall.length
  let gaps = gapsByColumn(wall)

  return minBrokenBricksFromGaps(wallHeight, gaps)
}


const minBrokenBricksModule = module.exports = minBrokenBricks
minBrokenBricksModule.gapsByColumn = gapsByColumn
minBrokenBricksModule.minBrokenBricksFromGaps = minBrokenBricksFromGaps
