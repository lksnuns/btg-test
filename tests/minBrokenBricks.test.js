const minBrokenBricks = require('../src/minBrokenBricks')
const gapsByColumn = minBrokenBricks.gapsByColumn
const minBrokenBricksFromGaps = minBrokenBricks.minBrokenBricksFromGaps


const wall = [[1, 2, 2, 1],
              [3, 1, 2],
              [1, 3, 2],
              [2, 4],
              [3, 1, 2],
              [1, 3, 1, 1]]

describe('minBrokenBricks', () => {
  it('returns the smallest number of broken bricks', () => {
    expect(minBrokenBricks(wall)).toBe(2)
  })
})

describe('gapsByColumn', () => {
  it('returns an array with amount of gaps of each column', () => {
    expect(gapsByColumn(wall)).toEqual([3, 1, 3, 4, 2])
  })
})

describe('minBrokenBricksFromGaps', () => {
  it('returns the smallest difference between wallHeight and gaps', () => {
    expect(minBrokenBricksFromGaps(6, [3, 1, 3, 4, 2])).toBe(2)
  })
})
