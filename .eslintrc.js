module.exports = {
    "env": {
        "commonjs": true,
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "rules": {
        'arrow-parens': ['error', 'as-needed'],
        'arrow-body-style': ['error', 'as-needed'],
        'comma-dangle': ['error', 'never'],
        indent: ['error', 2],
        'max-len': ['error', { code: 120, ignoreComments: true }],
        'no-multiple-empty-lines': ['error', { max: 3 }],
        'no-param-reassign': ['error', { props: false }],
        'no-unused-expressions': ['error', { allowTernary: true }],
        'no-unused-vars': ['error', { args: 'none' }],
        quotes: ['error', 'single', { avoidEscape: true, allowTemplateLiterals: true }],
        semi: ['error', 'never'],
        'react/jsx-one-expression-per-line': 'off',
    }
};
