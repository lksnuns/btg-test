## How To run

Build image
```sh
docker build -t bricks .
```

Update the `input/wall.txt` with the wall array.

Run script
```sh
docker run -v $(pwd)/input:/app/input -ti bricks
```

The script will read from `input/wall.txt` and calculate the smallest possible number of broken bricks

#### Run tests
```
docker run -ti bricks sh -c 'yarn jest'
```


## Análise de complexidade

A função `gapsByColumn()` faz a utilização de dois _loops_ aninhados, que definem sua complexidade como **O(n^2)**,
A função `minBrokenBricksFromGaps()` percorre apenas um _loop_, tendo complexidade **O(n)**,

Assim a função principal `minBrokenBricks()` que utiliza as 2 funções descritas acima, tem complexidade **O(n^2) + O(n)**.
Como O(n) pode ser desconsiderado na presença de O(n^2), podemos concluir que:

#### Complexidade final: O(n^2)
