FROM node:12.16.1-alpine3.11 as build-stage

ARG APP_PATH=/app

WORKDIR $APP_PATH
RUN apk add --update --no-cache bash


COPY . ./
RUN yarn install

RUN mkdir -p $APP_PATH/input
VOLUME $APP_PATH/input

CMD ["yarn", "start"]
